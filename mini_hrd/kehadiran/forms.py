from django.forms import ModelForm
from django import forms
from kehadiran.models import Izin

class IzinForm(ModelForm):
    class Meta:
        model = Izin
        fields = ['jenis_kehadiran', 'waktu_mulai', 'waktu_berhenti', 'alasan']
        labels = {
            'jenis_kehadiran':"Type",
            'waktu_mulai':"Start Date",
            'waktu_berhenti':"End Date",
            'alasan':'Reason',
        }
        error_messages={
            'jenis_kehadiran':{
                'required':'Please pick which type'
            },
            'waktu_mulai': {
                'required': 'Please pick start date'
            },
            'waktu_berhenti': {
                'required': 'Please pick end date'
            },
            'alasan': {
                'required': 'Please tell the reason'
            },
        }
        widgets = {
            'alasan': forms.Textarea(attrs={'cols':50, 'rows':10})
        }
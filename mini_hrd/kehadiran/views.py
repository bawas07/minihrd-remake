from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse

import json

from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib import colors

from karyawan.models import Karyawan
from kehadiran.models import Kehadiran, Izin
from kehadiran.forms import IzinForm

# Create your views here.

@login_required(login_url=settings.LOGIN_URL)
def daftar_hadir(request, bulan=0, tahun=0):
    #daftar_hadir = None
    if request.method == 'POST':
        bulan = request.POST['bulan']
        tahun = request.POST['tahun']
        daftar_hadir = Kehadiran.objects.filter(waktu__year=tahun, waktu__month=bulan, karyawan__id=request.session['karyawan_id'])
    else:
        if int(bulan) == 0 or int(tahun) == 0:
            daftar_hadir = Kehadiran.objects.filter(karyawan__id=request.session['karyawan_id'])
        else:
            daftar_hadir = Kehadiran.objects.filter(waktu__year = tahun, waktu__month = bulan, karyawan__id=request.session['karyawan_id'])

        paginator = Paginator(daftar_hadir, 5)
        page = request.GET.get('page')

        try:
            daftar_hadir=paginator.page(page)
        except PageNotAnInteger:
            daftar_hadir = paginator.page(1)
        except EmptyPage:
            daftar_hadir = paginator.page(paginator.num_pages)

    return render(request, 'new/daftar_hadir.html', {'daftar_hadir':daftar_hadir, 'bulan':bulan, 'tahun':tahun})

@login_required(login_url=settings.LOGIN_URL)
def pengajuan_izin(request):
    if request.method == 'POST':
        form_data = request.POST
        form = IzinForm(form_data)
        if form.is_valid():
            izin = Izin(
                karyawan = Karyawan.objects.get(id=request.session['karyawan_id']),
                jenis_kehadiran = request.POST['jenis_kehadiran'],
                waktu_mulai = request.POST['waktu_mulai'],
                waktu_berhenti = request.POST['waktu_berhenti'],
                alasan = request.POST['alasan'],
                disetujui = False,
            )
            izin.save()
            return redirect('/')
    else:
        form = IzinForm()

    return render(request, 'new/tambah_izin.html', {'form':form})

@login_required(login_url=settings.LOGIN_URL)
def daftar_izin(request):
    daftar_izin = Izin.objects.filter(karyawan__id=request.session['karyawan_id']).order_by('-waktu_mulai')
    paginator = Paginator(daftar_izin,5)
    page= request.GET.get('page')
    try:
        daftar_izin=paginator.page(page)
    except PageNotAnInteger:
        daftar_izin = paginator.page(1)
    except Emptypage:
        daftar_izin=paginator.page(paginator.num_pages)

    return render(request, 'new/daftar_izin.html', {'daftar_izin':daftar_izin})

@login_required(login_url=settings.LOGIN_URL)
def tampil_grafik(request, bulan=07, tahun=2017):
    temp_chart_data = []
    daftar_hadir = Kehadiran.objects.filter(waktu__year=tahun, waktu__month=bulan, karyawan__id=request.session['karyawan_id'])

    temp_chart_data.append({"x": "hadir", "a": daftar_hadir.filter(jenis_kehadiran='hadir').count() })
    temp_chart_data.append({"x": "izin", "a": daftar_hadir.filter(jenis_kehadiran='izin').count()})
    temp_chart_data.append({"x": "apla", "a": daftar_hadir.filter(jenis_kehadiran='alpa').count()})
    temp_chart_data.append({"x": "cuti", "a": daftar_hadir.filter(jenis_kehadiran='cuti').count()})

    chart_data = json.dumps({"data":temp_chart_data})

    return render(request, 'new/tampil_grafik.html', {'chart_data':chart_data, 'bulan':bulan, 'tahun':tahun})

@login_required(login_url=settings.LOGIN_URL)
def cetak_daftar_hadir(request, bulan, tahun):
    filename = "attendance_" + str(bulan) + "_" + str(tahun)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="' + filename + '.pdf"'

    #mengambil daftar kehadiran dan mengubahnya menjadi data untuk tabel
    data = Kehadiran.objects.filter(waktu__year=tahun, waktu__month=bulan, karyawan__id=request.session['karyawan_id'])
    table_data = []
    table_data.append(["Date", "Status", "Clock In", "Clock Out"])
    for x in data:
        table_data.append([x.waktu, x.jenis_kehadiran, x.jam_masuk, x.jam_pulang])

    doc = SimpleDocTemplate(response, pagesize=A4, rightMargin=72, leftMargin=72, topMargin=72, bottomMargin=18)
    styles = getSampleStyleSheet()

    table_style = TableStyle([
                               ('ALIGN',(1,1),(-2,-2),'RIGHT'),
                               ('FONT', (0, 0), (-1, 0), 'Helvetica-Bold'),
                               ('VALIGN',(0,0),(0,-1),'TOP'),
                               ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                               ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                           ])
    kehadiran_table = Table(table_data, colWidths=[doc.width/4.0]*2)
    kehadiran_table.setStyle(table_style)

    content = []
    content.append(Paragraph('Attendance for %s / %s'% (bulan, tahun), styles['Title']))
    content.append(Spacer(1,12))
    content.append(Paragraph('Below here is your attendance for %s / %s:' %(bulan, tahun), styles['Normal']))
    content.append(Spacer(1, 12))
    content.append(kehadiran_table)
    content.append(Spacer(1, 36))
    content.append(Paragraph('Acknowledge, ', styles['Normal']))
    content.append(Spacer(1, 48))
    content.append(
        Paragraph('______________________', styles['Normal']))

    # menghasilkan pdf untk di download
    doc.build(content)
    return response
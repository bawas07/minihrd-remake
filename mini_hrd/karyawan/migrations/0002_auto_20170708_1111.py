# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-07-08 04:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('karyawan', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='akun',
            name='jenis_akun',
            field=models.CharField(choices=[('employee', 'Employee'), ('admin', 'Administrator')], max_length=20),
        ),
        migrations.AlterField(
            model_name='karyawan',
            name='jenis_karyawan',
            field=models.CharField(choices=[('intern', 'Internship'), ('contract', 'Contract'), ('full time', 'Full Time')], max_length=10),
        ),
        migrations.AlterField(
            model_name='karyawan',
            name='jenis_kelamin',
            field=models.CharField(choices=[('male', 'Male'), ('female', 'Female')], max_length=10),
        ),
    ]
